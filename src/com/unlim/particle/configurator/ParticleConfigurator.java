package com.unlim.particle.configurator;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.particle.BatchedSpriteParticleSystem;
import org.andengine.entity.particle.SpriteParticleSystem;
import org.andengine.entity.particle.emitter.BaseParticleEmitter;
import org.andengine.entity.particle.emitter.PointParticleEmitter;
import org.andengine.entity.particle.emitter.RectangleParticleEmitter;
import org.andengine.entity.particle.initializer.AccelerationParticleInitializer;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.particle.modifier.ExpireParticleInitializer;
import org.andengine.entity.particle.modifier.ScaleParticleModifier;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.UncoloredSprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.util.FPSLogger;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.view.RenderSurfaceView;
import org.andengine.ui.activity.SimpleBaseGameActivity;


import android.graphics.Color;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

/**
 * (c) 2010 Nicolas Gramlich
 * (c) 2011 Zynga
 *
 * @author Nicolas Gramlich
 * @since 11:54:51 - 03.04.2010
 */
public class ParticleConfigurator extends SimpleBaseGameActivity {
	// ===========================================================
	// Constants
	// ===========================================================

	private static final int CAMERA_WIDTH = 800;
	private static final int CAMERA_HEIGHT = 480;

	// ===========================================================
	// Fields
	// ===========================================================

	private Camera mCamera;
	private BitmapTextureAtlas mBitmapTextureAtlas;
	private ITextureRegion mParticleTextureRegion;
	private Font fpsFont;
	
	
	/* Define the particle system properties */
	final float minSpawnRate = 25;
	final float maxSpawnRate = 50;
	float minXSpeed=200;
	int defaultMaxParticleCount = 150;
	float height=50;
	float expDefaultTime=2f;
	float defStartScale = 0.5f;
	float defMaxScale = 1.5f;
	//private BatchedSpriteParticleSystem particleSystem;
	private SpriteParticleSystem particleSystem;
	private BaseParticleEmitter particleEmitter;
	// ===========================================================
	// Constructors
	// ===========================================================

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	public EngineOptions onCreateEngineOptions() {

		this.mCamera = new Camera(0, 0, ParticleConfigurator.CAMERA_WIDTH, ParticleConfigurator.CAMERA_HEIGHT);

		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(ParticleConfigurator.CAMERA_WIDTH, ParticleConfigurator.CAMERA_HEIGHT), this.mCamera);
	}
	@Override
	protected void onSetContentView() {
	    final FrameLayout frameLayout = new FrameLayout(this);
	    final FrameLayout.LayoutParams frameLayoutLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

	    this.mRenderSurfaceView = new RenderSurfaceView(this);
	    mRenderSurfaceView.setRenderer(mEngine, this);
	    final FrameLayout.LayoutParams surfaceViewLayoutParams = new FrameLayout.LayoutParams(super.createSurfaceViewLayoutParams());
	    frameLayout.addView(this.mRenderSurfaceView, surfaceViewLayoutParams);

	    //Create any other views you want here, and add them to the frameLayout.
	    addViews(frameLayout);

	    this.setContentView(frameLayout, frameLayoutLayoutParams);
	}
	private void addViews(FrameLayout frameLayout)
	{
	    LinearLayout ll=new LinearLayout(this);
	    ll.setOrientation(LinearLayout.VERTICAL);
	    ll.setPadding(15, 45, 0, 0);
	    frameLayout.addView(ll,FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
	    
	    addSeek(ll,"Кол-во",500, defaultMaxParticleCount, 1,new onStopSeekListener()
		{
			
			@Override
			public void onStopTrackingTouch(float value)
			{
				particleSystem.detachSelf();
				particleSystem.dispose();
				
				defaultMaxParticleCount=(int) value;
				createAndAttachParticle(getEngine().getScene(),defaultMaxParticleCount, height, minXSpeed, expDefaultTime, defMaxScale, defStartScale);
			}

		});
	    
	    addSeek(ll,"Верт. угол",300,(int)height, 1, new onStopSeekListener()
		{
			
			@Override
			public void onStopTrackingTouch(float value)
			{
				particleSystem.detachSelf();
				particleSystem.dispose();
				height=value;
				
				createAndAttachParticle(getEngine().getScene(), defaultMaxParticleCount, height, minXSpeed, expDefaultTime, defMaxScale, defStartScale);
			}

		});
	    addSeek(ll,"X скороcть",1000,(int)minXSpeed, 1, new onStopSeekListener()
		{
			
			@Override
			public void onStopTrackingTouch(float value)
			{
				particleSystem.detachSelf();
				particleSystem.dispose();
				minXSpeed=value;
				
				createAndAttachParticle(getEngine().getScene(), defaultMaxParticleCount, height,minXSpeed, expDefaultTime, defMaxScale, defStartScale);
			}

		});
	    addSeek(ll,"Время жизни",5, expDefaultTime, .1f, new onStopSeekListener()
		{
			
			@Override
			public void onStopTrackingTouch(float value)
			{
				particleSystem.detachSelf();
				particleSystem.dispose();
				expDefaultTime=value;
				
				createAndAttachParticle(getEngine().getScene(), defaultMaxParticleCount, height, minXSpeed, expDefaultTime, defMaxScale, defStartScale);
			}

		});
	    addSeek(ll,"Размер в начале",7, defStartScale, .1f, new onStopSeekListener()
		{
			
			@Override
			public void onStopTrackingTouch(float value)
			{
				particleSystem.detachSelf();
				particleSystem.dispose();
				defStartScale=value;
				
				createAndAttachParticle(getEngine().getScene(), defaultMaxParticleCount, height, minXSpeed, expDefaultTime, defMaxScale, defStartScale);
			}

		});
	    addSeek(ll,"Макс размер",7, defMaxScale, .1f, new onStopSeekListener()
		{
			
			@Override
			public void onStopTrackingTouch(float value)
			{
				particleSystem.detachSelf();
				particleSystem.dispose();
				defMaxScale=value;
				
				createAndAttachParticle(getEngine().getScene(), defaultMaxParticleCount, height, minXSpeed, expDefaultTime, defMaxScale, defStartScale);
			}

		});
	}
	private void addSeek(LinearLayout layout, String label, int max, float defValue, final float step, final onStopSeekListener listener)
	{
		LinearLayout ll = new LinearLayout(this);
		ll.setOrientation(LinearLayout.HORIZONTAL);
		
		TextView tv = new TextView(this);
		tv.setText(label);
		
		final TextView seekValue = new TextView(this);
		seekValue.setText(String.valueOf(defValue));
		
		SeekBar seek=new SeekBar(this);
		seek.setMax((int) (max/step));
		seek.setProgress((int) (defValue/step));
		seek.setOnSeekBarChangeListener(new OnSeekBarChangeListener()
		{
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar)
			{
				listener.onStopTrackingTouch(seekBar.getProgress()*step);
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar)
			{
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
			{
				seekValue.setText(String.valueOf(progress*step));
			}
		});
		
		ll.addView(tv, FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
		ll.addView(seek, 500, FrameLayout.LayoutParams.WRAP_CONTENT);
		ll.addView(seekValue, FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
		
		layout.addView(ll, FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
	}
	@Override
	protected void onCreateResources() {
		//BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

		this.mBitmapTextureAtlas = new BitmapTextureAtlas(this.getTextureManager(), 32, 32, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.mParticleTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas, this, "particle_point_green.png", 0, 0);

		this.mBitmapTextureAtlas.load();
		
		BitmapTextureAtlas mFontTexture = new BitmapTextureAtlas(this.getTextureManager(), 128, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		fpsFont = FontFactory.create(getFontManager(), mFontTexture, 20,Color.LTGRAY);
		mFontTexture.load();
		fpsFont.load();
	}

	@Override
	protected Scene onCreateScene() {
		//this.mEngine.registerUpdateHandler(new FPSLogger());

		final Scene scene = new Scene();
		
		createAndAttachParticle(scene, defaultMaxParticleCount, height, minXSpeed, expDefaultTime, defMaxScale, defStartScale);
		
		addHUD();
		
		return scene;
	}
	private void createAndAttachParticle(Scene scene, int maxParticleCount, float height, float minXSpeed, float expTime, 
			float maxScale, float startScale)
	{
		//particleEmitter = new PointParticleEmitter(500, 350);
		particleEmitter = new RectangleParticleEmitter(CAMERA_WIDTH, CAMERA_HEIGHT/2, 1, CAMERA_HEIGHT);
		/*particleSystem = new BatchedSpriteParticleSystem(
				particleEmitter, minSpawnRate, maxSpawnRate, maxParticleCount,
				mParticleTextureRegion, mEngine.getVertexBufferObjectManager());*/
		
		particleSystem = new SpriteParticleSystem(particleEmitter, minSpawnRate, maxSpawnRate, maxParticleCount, this.mParticleTextureRegion ,
		        this.getVertexBufferObjectManager());
		
		scene.setOnSceneTouchListener(new IOnSceneTouchListener() {
			@Override
			public boolean onSceneTouchEvent(final Scene pScene, final TouchEvent pSceneTouchEvent) {
				particleEmitter.setCenter(pSceneTouchEvent.getX(), pSceneTouchEvent.getY());
				return true;
			}
		});
		
		particleSystem.addParticleInitializer(new VelocityParticleInitializer<Sprite>(-minXSpeed, -minXSpeed+100f, height, -height));
		/* Add an acceleration initializer to the particle system */
		//particleSystem.addParticleInitializer(new AccelerationParticleInitializer<UncoloredSprite>(-minXSpeed, -minXSpeed+100f, height, -height));
		/* Add an expire initializer to the particle system */
		particleSystem.addParticleInitializer(new ExpireParticleInitializer<Sprite>(expTime));
		/* Add a particle modifier to the particle system */
		particleSystem.addParticleModifier(new ScaleParticleModifier<Sprite>(0f, expTime, startScale, maxScale));
		/* Attach the particle system to the Scene */

		scene.attachChild(particleSystem);
	}
	private void addHUD()
	{
		HUD hud=new HUD();
		final Text tFPS = new Text(20, 20, fpsFont, "Fps: ?", "Fps: XXXX".length(), getVertexBufferObjectManager());
		hud.attachChild(tFPS);
		getEngine().registerUpdateHandler(new FPSLogger(0.5f) {
			@Override
			protected void onLogFPS() {
				final String fpsString = String.format("FPS: %.2f", this.mFrames / this.mSecondsElapsed);
				tFPS.setText(fpsString);
			}
		});
		getEngine().getCamera().setHUD(hud);
	}

	interface onStopSeekListener
	{
		public void onStopTrackingTouch(float value);
	}
	// ===========================================================
	// Methods
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
}
